package com.am.nail.art.designs

class CustomException(message: String) : Exception(message)
